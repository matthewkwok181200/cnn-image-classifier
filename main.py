from tensorflow import keras
import tensorflow as tf
from tensorflow.keras.models import Sequential
from google.colab import drive
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import pathlib
from tensorflow.keras import layers
import matplotlib.pyplot as plt
import PIL
import numpy


# download image and produce dataset 

drive.mount("/content/drive", force_remount = True)

data_dir = pathlib.Path("/content/drive/My Drive/Colab Notebooks/flower_photo/")

batch_size = 32
Image_height = (180)
Image_width = (180)



train_ds = tf.keras.utils.image_dataset_from_directory(
    data_dir, 
    validation_split = 0.2,
    batch_size = batch_size,
    seed = 123,
    image_size = (Image_height, Image_width),
    subset = "training"

)



val_ds = train_ds = tf.keras.utils.image_dataset_from_directory(
    data_dir, 
    validation_split = 0.2,
    batch_size = batch_size,
    seed = 123,
    image_size = (Image_height, Image_width),
    subset = "validation"

)


# model training

model = Sequential([
  layers.Rescaling(1./255, input_shape=(Image_height, Image_width, 3)),
  layers.Conv2D(16, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(32, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(64, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Flatten(),
  layers.Dropout(0.2),
  layers.Dense(128, activation='relu'),
  layers.Dense(int(len(class_names)))
])

model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

AI = model.fit(
    train_ds, 
    validation_data = val_ds,
    epochs = 5
)


# plotting results 

val_loss = model.history["val_loss"]
loss = model.history["loss"]
acc = model.history["accuracy"]
val_acc = model.history["val_accuracy"]
epochs = 5

num_epochs = range(epochs)


plt.figure(figsize=(8,8))
plt.plot(num_epochs, val_loss, label =("val_loss"))
plt.plot(num_epochs, loss, label =("loss"))
plt.title("loss")


plt.figure(figsize=(8,8))
plt.plot(num_epochs, val_acc, label =("val_acc"))
plt.plot(num_epo


# prediction

pic = pathlib.Path("/content/drive/My Drive/Colab Notebooks/DandelionFlower.jpg")

img = tf.keras.utils.load_img(
    pic, target_size=(Image_height, Image_width)
) 

img_array = tf.keras.utils.img_to_array(img)
img_array = tf.expand_dims(img_array,0)



prediction = model.predict(img_array)

max = numpy.argmax(prediction)

decision = class_names[max]
  
print("the picture is a " + decision)
